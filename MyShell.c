#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<linux/limits.h>
#include<string.h>
#include<sys/wait.h>
#include<sys/types.h>
#include "LineParser.h"

#define TRUE 1

void execute(cmdLine *pcmd){
	int infld, help1, help2, outfld;
	if(pcmd->inputRedirect != NULL)
	{
		help1 = dup(0);
		if(help1 != -1) //if dup worked
		{
			close(0);
		infld = open(pcmd->inputRedirect, 0);
		}
	}
	if(pcmd->outputRedirect != NULL)
	{
		help2 = dup(1);
		if(help2 != -1) //if dup worked
		{
			close(1);
		outfld = open(pcmd->outputRedirect, 1);
		}
	}
	execvp(pcmd->arguments[0], pcmd->arguments);
	perror("Something went wrong");
	if(help1 != -1)
	{
		close(infld);
		infld = dup(help1);
	if(infld == 0) close(infld);
	}
	if(help2 != -1)
	{
		close(outfld);
		outfld = dup(help2);
	if(outfld == 1) close(outfld);
	}
}

int main(){
	char Path[PATH_MAX], input[2048];
	int cpid, mone = 0, i;
	char arr[15][2048];
	cmdLine *pcmd;
	printf("\t\t\tWELCOME TO MYSHELL\n");
	while(TRUE)
	{
		getcwd(Path, PATH_MAX);
		printf("%s: ", Path);
		fgets(input,sizeof(input),stdin);
		if(strcmp(input,"quit\n") == 0)
		{
			printf("\t\t\tBye !\n");
			exit(0);
		}
		if(mone <= 14)
		{
			strcpy(arr[mone], input);
			mone++;	
		}
		else mone = 0;
		if(input[0] == '!') 
		{
			strcpy(input, arr[atoi(input+1)]);
		}
		pcmd = parseCmdLines(input);
		cpid = fork();
		if(cpid >= 0)
		{
			if(cpid == 0)
			{ 
				if(strcmp(pcmd->arguments[0], "history") == 0) for(i = 0; strcmp(arr[i], "") && i<=14; i++) printf("- %s", arr[i]);
				else if(strcmp(pcmd->arguments[0], "myecho") == 0) printf("\t\t\t%s", input+7);
				else if(strcmp(pcmd->arguments[0], "cd") == 0) chdir(pcmd->arguments[1]);
				else execute(pcmd);
				exit(1);
			}
			else
			{
				waitpid(-1, &cpid, 0);
			}
		}
		else
		{
		perror("Something went wrong");
		}
	}
	freeCmdLines(pcmd);
return 0;
}
