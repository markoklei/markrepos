#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

int main()
{
	int cpid;
	cpid = fork();
	if(cpid >= 0)
	{
		if(cpid == 0)
		{
			while(1)
			{
				printf("I'm still alive ");
			}		
		}
		else
		{
			sleep(1);
			printf("Dispatching\n");
			kill(cpid, SIGKILL);
			printf("Dispatched\n");
		}
	}
	return 0;
}
