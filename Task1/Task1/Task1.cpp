#include <Windows.h>
#include <stdio.h>

int main(int argc, char** argv)
{
	DWORD num = atoi(argv[1]);
	HANDLE hprocess = OpenProcess(PROCESS_TERMINATE, TRUE, num);
	if (hprocess == NULL)
	{
		printf("Failed to open process with Error Code :%u\nLook it up in error lookup.\n", GetLastError());
		system("pause");
		return 1;
	}
	TerminateProcess(hprocess, 0);
	CloseHandle(hprocess);	
	system("pause");
	return 1;
}