#include <Windows.h>

typedef void(_cdecl *PLUGIN_FUNC)(const char* msg);

int main(int argc, char** argv)
{
	HMODULE WINAPI hModule = LoadLibrary("tui_plugin.dll");
	PLUGIN_FUNC output_message = (PLUGIN_FUNC)GetProcAddress(hModule, "output_message");
	output_message(argv[1]);
	FreeLibrary(hModule);
	system("pause");
	return 1;
}