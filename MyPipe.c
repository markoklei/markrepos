#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/wait.h>
int main()
{
	int cpid, err;
	int Piper[2];
	char str[] = "magshimim";
	err = pipe(Piper);
	cpid = fork();
	if(cpid >= 0)
	{
		if(cpid == 0)
		{
			if(err == -1)
			{
				printf("pipe err\n");
				exit(0);
			}
			err = close(Piper[0]);
			if(err == -1)
			{ 
			printf("pipe err\n");
			exit(1);
			}	
			write(Piper[1], str, strlen(str));	
			close(Piper[1]);
		}
		else
		{
			err = close(Piper[1]);
			if(err == -1) printf("pipe err\n");
			write(STDOUT_FILENO, "\t", 1);
			while(read(Piper[0], str, 1) > 0)
			write(STDOUT_FILENO, str, 1);
		        write(STDOUT_FILENO, "\n", 1);
			close(Piper[0]);
			wait(NULL);
		}
	}
	else
	{
		perror("Something went wrong");
	}
	return (0);
}
