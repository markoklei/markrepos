#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#define SIZE 256

using namespace std;

int main(int argc, char** argv)
{
	wchar_t buffer[SIZE];
	GetModuleFileNameW(NULL, buffer, sizeof(buffer));
	//wcout << buffer << endl;
	//printf("%s\n", filename);
	printf("%ls\n", buffer);
	DeleteFileW(buffer);
	printf("Error number: %d\n", GetLastError()); //Prints out 5 -> Error Lookup -> Access is denied.
	system("pause");
	return 0;
}