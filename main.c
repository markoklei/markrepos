#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.h"

#define STDOUT 1
#define STDIN 0

void exitFunc()
{
	syscall(SYS_exit, 1);
}
void printid()
{
	syscall(SYS_write,STDOUT_FILENO,__itoa(getuid()), slen(__itoa(getuid())));
	syscall(SYS_write,STDOUT_FILENO,"\n",slen("\n"));
}
void printuptime()
{
	struct sysinfo info;
	sysinfo(&info);
syscall(SYS_write,STDOUT,__itoa(info.uptime/3600),slen(__itoa(info.uptime/3600)));
syscall(SYS_write,STDOUT," :",slen(" :"));
syscall(SYS_write,STDOUT,__itoa(info.uptime%3600/60), slen(__itoa(info.uptime%3600/60)));
syscall(SYS_write,STDOUT," :",slen(" :"));
syscall(SYS_write,STDOUT,__itoa(info.uptime%60),slen(__itoa(info.uptime%60)));
syscall(SYS_write,STDOUT,"\n",slen("\n"));
syscall(SYS_write,STDOUT,"process:", slen("process:"));
syscall(SYS_write,STDOUT,__itoa(info.procs),slen(__itoa(info.procs)));
syscall(SYS_write,STDOUT,"\n",slen("\n"));
}
int main()
{
	int i,num;
	char option[2] = "";
	fun_desc menu[4];
	menu[0].name = "Press 1 to exit\n";
	menu[0].fun = exitFunc;
	menu[1].name = "Press 2 to print id\n";
	menu[1].fun = printid;
	menu[2].name = "Press 3 to uptime\n";
	menu[2].fun = printuptime;
while(1)
{
	for(i=0; i<3; i++)
	{
	syscall(SYS_write, STDOUT, menu[i].name, slen(menu [i].name));
	}
	syscall(SYS_read, STDOUT, option, 2);
	num = atoi(option);
	switch(num)
	{
		case 1: menu[0].fun();break;
		case 2: menu[1].fun();break;
		case 3: menu[2].fun();break;
	}
}
}
