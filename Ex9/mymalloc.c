#include <unistd.h> // func sbrk
#include <stdlib.h>

static struct metadata_block start.size = 0;
static struct metadata_block start.next = NULL;
static struct metadata_block start.free = 1;

struct metadata_block
{
	size_t size;
	struct metadata_block *next;
	int free;
};
typedef struct metadata_block * p_block;

void free(struct *p_block)
{
	p_block->free = 1;
	//NO need to return, it is pointer.
}
void *my_malloc(size_t size) // size_t is unsigned 
{
	void *help = NULL;
	int i,count;
	rlsize = sizeof(size+sizeof(*p_block));
		if(*sbrk(rlsize) == -1){
			perror("oh no");
			return help;
		}
	while(start.next != NULL)
	{
		count++;
		start = start->next;	
	}
	
	p_block->size = rlsize;
	p_block = sbrk(rlsize);
	return (p_block + sizeof(p_block));//jump over struct straight to data.
	 //sbrk changes the place where the process's data segment ends.
}
